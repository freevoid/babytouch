﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BabyTouch.DataModel
{
    public class SampleDataSource
    {
        public List<Group> SampleGroups
        {
            get
            {
                return GetSampleGroups();
            }
        }

        private DictionaryDataSource _dataSource = new DictionaryDataSource();

        public SampleDataSource()
        {
            _dataSource.RegisterModel<Group>();
            _dataSource.RegisterModel<ItemClass>();
            _dataSource.RegisterModel<ItemInstance>();
            var groups = _dataSource.GetManager<Group>();
            var classes = _dataSource.GetManager<ItemClass>();
            var instances = _dataSource.GetManager<ItemInstance>();
            groups.Add(new Group
            {
                Id = "Animals",
                Title = "Animals"
            });

            classes.Add(new ItemClass
            {
                Id = "Cat",
                Title = "Cat",
                GroupId = "Animals",
                MainInstanceId = "Cat01"
            });
            classes.Add(new ItemClass
            {
                Id = "Dog",
                Title = "Dog",
                GroupId = "Animals",
                MainInstanceId = "Dog01"
            });

            instances.Add(new ItemInstance
            {
                Id = "Cat01",
                ClassId = "Cat",
                ImageUri = "ms-appx:///DefaultContent/Images/scaled/cat-01.jpg",
                ThumbnailUri = "ms-appx:///DefaultContent/Thumbnails/thumb_cat-01.jpg",
                SoundUri = "ms-appx:///DefaultContent/Sounds/Cat_meow_021.wav",
                Copyright = "By John Doe, 2012"
            });
            instances.Add(new ItemInstance
            {
                Id = "Dog01",
                ClassId = "Dog",
                ImageUri = "ms-appx:///DefaultContent/Images/scaled/dog-01.jpg",
                ThumbnailUri = "ms-appx:///DefaultContent/Thumbnails/thumb_dog-01.jpg",
                SoundUri = "ms-appx:///DefaultContent/Sounds/Dog_080.wav"
            });

            //_dataSource.Regroup();
        }

        public List<Group> GetSampleGroups()
        {
            return new List<Group>(_dataSource.GetManager<Group>().GetAll());
            //yield return g;
        }
    }
}
