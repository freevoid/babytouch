﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BabyTouch.DataModel.Contracts;

namespace BabyTouch.DataModel.InMemory
{
    class DictionaryDataManager<T> : IDataManager<T> where T : IDataModelProto, new()
    {
        private Dictionary<object, T> _dataMap;
        public IDataSource DataSource { get; private set; }

        public DictionaryDataManager(IDataSource parent)
        {
            this._dataMap = new Dictionary<object, T>();
            this.DataSource = parent;
        }

        public T GetById(object uniqueId)
        {
            T value;
            this._dataMap.TryGetValue(uniqueId, out value);
            return value;
        }

        public IEnumerable<T> GetAll()
        {
            return _dataMap.Values;
        }

        public T Add(T instance)
        {
            _dataMap[instance.GetUntypedPrimaryKey()] = instance;
            instance.DataSource = DataSource;
            return instance;
        }
    }

    abstract public class DictionaryDataSourceBase : IDataSource
    {
        protected Dictionary<Type, object> dataManagers = new Dictionary<Type, object>();

        public IEnumerable<Type> GetRegisteredModels()
        {
            return dataManagers.Keys;
        }

        public void RegisterModel<T>() where T : IDataModelProto, new()
        {
            dataManagers[typeof(T)] = new DictionaryDataManager<T>(this);
        }

        public IDataManager<T> GetManager<T>() where T : IDataModelProto, new()
        {
            return (IDataManager<T>) dataManagers[typeof(T)];
        }

        public bool NeedToBootstrap
        {
            get
            {
                return false;
            }
        }

        public void InitializeStorage()
        {
        }
    }
}
