﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using BabyTouch.DataModel.Contracts;
using Newtonsoft.Json;

namespace BabyTouch.DataModel
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Group : CommonDataModel
    {
        public PersistentQuery<ItemClass> Classes { get; private set; }
        public PersistentQuery<ItemClass> AllClasses { get; private set; }

        public Group()
        {
            Classes = new PersistentQuery<ItemClass>(this, e => e.GroupId == this.Id && e.IsEnabled == true);
            AllClasses = new PersistentQuery<ItemClass>(this, e => e.GroupId == this.Id);
        }
    }
}
