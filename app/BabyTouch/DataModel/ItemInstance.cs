﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace BabyTouch.DataModel
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ItemInstance : CommonDataModel
    {
        [JsonProperty]
        public string ClassId { get; set; }
        public ItemClass Class {
            get
            {
                return DataSource.GetManager<ItemClass>().GetById(ClassId);
            }
            set
            {
                ClassId = value.Id;
            }
        }

        [JsonProperty]
        public string SpokenTitleUri { get; set; }

        private string _imageUri = null;
        [JsonProperty]
        public string ImageUri
        {
            get
            {
                if (_imageUri != null)
                {
                    return _imageUri;
                }
                return Class.MainInstanceId != Id ? Class.MainInstance.ImageUri : null;
            }
            set
            {
                _imageUri = value;
            }
        }

        private string _thumbnailUri = null;
        [JsonProperty]
        public string ThumbnailUri
        {
            get
            {
                if (_thumbnailUri != null)
                {
                    return _thumbnailUri;
                }
                return Class.MainInstanceId != Id ? Class.MainInstance._thumbnailUri : null;
            }
            set
            {
                _thumbnailUri = value;
            }
        }

        private string _soundUri = null;
        [JsonProperty]
        public string SoundUri {
            get
            {
                if (_soundUri != null)
                {
                    return _soundUri;
                }
                return Class.MainInstanceId != Id ? Class.MainInstance.SoundUri : null;
            }
            set
            {
                _soundUri = !String.IsNullOrEmpty(value) ? value : null;
            }
        }

        public bool ShouldSerializeSoundUri()
        {
            return _soundUri != null;
        }

        [JsonProperty]
        public string Copyright { get; set; }
    }
}
