﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BabyTouch.DataModel.Contracts
{
    public interface IDataManager<T> where T : new()
    {
        IDataSource DataSource { get; }
        T GetById(object uniqueId);
        IEnumerable<T> GetAll();
        T Add(T instance);
    }
}
