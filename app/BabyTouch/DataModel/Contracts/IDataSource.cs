﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace BabyTouch.DataModel.Contracts
{
    public interface IDataSource
    {
        void RegisterModel<T>() where T : IDataModelProto, new();
        IDataManager<T> GetManager<T>() where T : IDataModelProto, new();
        IEnumerable<Type> GetRegisteredModels();

        bool NeedToBootstrap { get; }
        void InitializeStorage();
    }
}
