﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace BabyTouch.DataModel.Contracts
{
    public interface IContentDumper
    {
        Task<bool> DumpModelAsync<T>(IDataSource source) where T : IDataModelProto, new();
    }

    public interface IContentLoader
    {
        Task<bool> LoadModelAsync<T>(IDataSource target) where T : IMergeableDataModel, new();
    }

    public interface IPersistenceLayer : IContentDumper, IContentLoader
    {
    }
}
