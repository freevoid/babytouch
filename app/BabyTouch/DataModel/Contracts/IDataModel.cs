﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BabyTouch.DataModel.Contracts
{
    public interface IDataModelProto
    {
        Type PrimaryKeyType { get; }
        IDataSource DataSource { get; set; }

        object GetUntypedPrimaryKey();
    }

    public interface IMergeableDataModel : IDataModelProto
    {
        void MergeInplace<T>(T obj);
    }

    public interface IDataModel<KeyType>: IDataModelProto
    {
        KeyType PrimaryKey { get; }
    }
}
