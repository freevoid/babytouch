﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;

namespace BabyTouch.DataModel
{
    public static class GetText
    {
        private static ResourceLoader _loader = new ResourceLoader();

        public static string _(string key)
        {
            var localized = _loader.GetString(key);
            return String.IsNullOrEmpty(localized) ? key : localized;
        }
    }
}
