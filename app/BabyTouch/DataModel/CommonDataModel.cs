﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using BabyTouch.DataModel.Contracts;

namespace BabyTouch.DataModel
{
    public abstract class CommonDataModel : BabyTouch.Common.BindableBase, IDataModel<string>, IMergeableDataModel
    {
        private string _id = string.Empty;
        [JsonProperty]
        public string Id
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }

        public string PrimaryKey { get { return _id; } }

        public object GetUntypedPrimaryKey()
        {
            return PrimaryKey;
        }

        public Type PrimaryKeyType { get { return typeof(string); } }

        private bool _isEnabled = true;
        public bool IsEnabled {
            get { return _isEnabled; }
            set { SetProperty(ref _isEnabled, value); }
        }

        private string _title = string.Empty;
        [JsonProperty]
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public string LocalizedTitle
        {
            get
            {
                return GetText._(_title);
            }
        }

        public virtual void MergeInplace<T>(T obj)
        {
            var commonObj = (obj as CommonDataModel);
            if (commonObj == null)
            {
                return;
            }

            IsEnabled = commonObj.IsEnabled;
        }

        public IDataSource DataSource { get; set; }
    }
}
