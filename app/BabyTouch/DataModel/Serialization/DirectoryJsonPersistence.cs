﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Storage;
using BabyTouch.DataModel.Contracts;
using Newtonsoft.Json;

namespace BabyTouch.DataModel.Serialization
{
    public static class StorageUtil
    {

        public static Tuple<IStorageFolder, string> ParseApplicationUri(string uriString)
        {
            IStorageFolder baseFolder;
            string relativePart;

            var uri = new Uri(uriString);

            if (uri.Scheme == "ms-appx")
            {
                baseFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;
                relativePart = uri.AbsolutePath;
            }
            else if (uri.Scheme == "ms-appdata")
            {
                var pathParts = uri.AbsolutePath.TrimStart('/').Split(new char[] {'/'}, 2);
                var folderType = pathParts[0];
                if (pathParts.Length == 2)
                {
                    relativePart = pathParts[1];
                }
                else
                {
                    relativePart = String.Empty;
                }

                if (folderType == "local")
                {
                    baseFolder = ApplicationData.Current.LocalFolder;
                }
                else if (folderType == "roaming")
                {
                    baseFolder = ApplicationData.Current.RoamingFolder;
                }
                else if (folderType == "temp")
                {
                    baseFolder = ApplicationData.Current.TemporaryFolder;
                }
                else
                {
                    throw new ArgumentException(
                        String.Format("Expected local, roaming or temp as first level directory, got {0]", folderType), "uriString");
                }
            }
            else
            {
                return null;
            }

            return new Tuple<IStorageFolder, string>(baseFolder, relativePart);
        }

        public static async Task<IStorageFolder> TryOpenNestedDirectoryAsync(IStorageFolder baseFolder, string relativePath)
        {
            foreach (var part in relativePath.Trim('/').Split('/'))
            {
                try
                {
                    baseFolder = await baseFolder.GetFolderAsync(part);
                }
                catch (System.IO.FileNotFoundException)
                {
                    return null;
                }
            }
            return baseFolder;
        }

        public static async Task<IStorageFolder> CreateNestedDirectoryAsync(IStorageFolder baseFolder, string relativePath)
        {
            foreach (var part in relativePath.Trim('/').Split('/'))
            {
                baseFolder = await baseFolder.CreateFolderAsync(part, CreationCollisionOption.OpenIfExists);
            }
            return baseFolder;
        }
    }

    public class DirectoryJsonLoader : IContentLoader
    {
        protected IStorageFolder _baseFolder;

        protected DirectoryJsonLoader(IStorageFolder baseFolder)
        {
            _baseFolder = baseFolder;
        }

        public static async Task<DirectoryJsonLoader> Create(string baseUri)
        {
            var baseFolder = await GetStorageFolderAsync(baseUri);
            return new DirectoryJsonLoader(baseFolder);
        }

        public static async Task<IStorageFolder> GetStorageFolderAsync(string baseUri)
        {
            var parts = StorageUtil.ParseApplicationUri(baseUri);
            if (parts == null)
            {
                throw new ArgumentException("Bad application URI", "baseUri");
            }
            var rootFolder = parts.Item1;
            var relativePath = parts.Item2;
            var baseFolder = await StorageUtil.CreateNestedDirectoryAsync(rootFolder, relativePath);
            if (baseFolder == null)
            {
                throw new Exception("Cannot create a directory to dump data");
            }
            return baseFolder;
       }

        public async Task<bool> LoadModelAsync<T>(IDataSource dataSource) where T : IMergeableDataModel, new()
        {
            StorageFile file;
            try
            {
                file = await GetFixtureForTypeAsync<T>();
            }
            catch (System.IO.FileNotFoundException)
            {
                return false;
            }
            return await LoadModel<T>(file, dataSource);
        }

        private IAsyncOperation<StorageFile> GetFixtureForTypeAsync<T>()
        {
            return _baseFolder.GetFileAsync(typeof(T).Name + ".json");
        }

        private async Task<bool> LoadModel<T>(StorageFile file, IDataSource dataSource) where T : IMergeableDataModel, new()
        {
            var manager = dataSource.GetManager<T>();
            var content = await FileIO.ReadTextAsync(file);
            var objects = await JsonConvert.DeserializeObjectAsync<List<T>>(content);
            if (objects == null)
            {
                return false;
            }
            foreach (T obj in objects)
            {
                var existing = manager.GetById(obj.GetUntypedPrimaryKey());
                if (existing != null)
                {
                    ((IMergeableDataModel)existing).MergeInplace(obj);
                }
                else
                {
                    manager.Add(obj);
                }
            }
            return true;
        }
    }

    public class DirectoryJsonPersistence : DirectoryJsonLoader, IPersistenceLayer
    {
        public new static async Task<DirectoryJsonPersistence> Create(string baseUri)
        {
            var baseFolder = await GetStorageFolderAsync(baseUri);
            return new DirectoryJsonPersistence(baseFolder);
        }

        protected DirectoryJsonPersistence(IStorageFolder baseFolder) : base(baseFolder) { }

        public async Task<bool> DumpModelAsync<T>(IDataSource dataSource) where T : IDataModelProto, new()
        {
            return await DumpModel<T>(await GetOrCreateFixtureForTypeAsync<T>(), dataSource);
        }

        private IAsyncOperation<StorageFile> GetOrCreateFixtureForTypeAsync<T>()
        {
            return _baseFolder.CreateFileAsync(typeof(T).Name + ".json", CreationCollisionOption.OpenIfExists);
        }

        private async Task<bool> DumpModel<T>(StorageFile file, IDataSource dataSource) where T : IDataModelProto, new()
        {
            var manager = dataSource.GetManager<T>();
            var content = await JsonConvert.SerializeObjectAsync(manager.GetAll().ToList());
            await WriteTextWithBackupAsync(file, content);
            return true;
        }

        private async Task WriteTextWithBackupAsync(StorageFile file, string contents)
        {
            var backup = await file.CopyAsync(_baseFolder, file.Name + ".backup", NameCollisionOption.ReplaceExisting);
            bool needRevert = false;
            try
            {
                await FileIO.WriteTextAsync(file, contents);
            }
            catch
            {
                needRevert = true;
            }

            if (needRevert)
            {
                await backup.RenameAsync(file.Name, NameCollisionOption.ReplaceExisting);
            }
            else
            {
                await backup.DeleteAsync();
            }
        }
    }
}
