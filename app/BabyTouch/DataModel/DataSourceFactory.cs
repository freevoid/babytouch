﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BabyTouch.DataModel.Contracts;

namespace BabyTouch.DataModel
{
    public interface IDataSourceManager
    {
        IDataSource GetDataSource();
        Task InitializeAsync();
        Task<bool> BootstrapFromDefaultAsync();
        Task<bool> LoadAsync();
        Task<bool> DumpAsync();
    }

    public class DataSourceManager : IDataSourceManager
    {
        private object _thisLock = new object();
        private IDataSource _dataSource;
        private IPersistenceLayer _persistenceLayer;
        private static DataSourceManager _manager = new DataSourceManager();

        public static IDataSourceManager Instance { get { return _manager; } }
        public static IDataSource DataSource { get { return _manager.GetDataSource(); } }

        public async Task<bool> LoadAsync()
        {
            return await _persistenceLayer.LoadModelAsync<Group>(_dataSource) &&
                   await _persistenceLayer.LoadModelAsync<ItemClass>(_dataSource) &&
                   await _persistenceLayer.LoadModelAsync<ItemInstance>(_dataSource);
        }

        public async Task<bool> DumpAsync()
        {
            return await _persistenceLayer.DumpModelAsync<Group>(_dataSource) &&
                   await _persistenceLayer.DumpModelAsync<ItemClass>(_dataSource) &&
                   await _persistenceLayer.DumpModelAsync<ItemInstance>(_dataSource);
        }

        public async Task<bool> BootstrapFromDefaultAsync()
        {
            var loader = await GetDefaultLoaderAsync();
            return await loader.LoadModelAsync<Group>(_dataSource) &&
                   await loader.LoadModelAsync<ItemClass>(_dataSource) &&
                   await loader.LoadModelAsync<ItemInstance>(_dataSource);
        }

        public async Task InitializeAsync()
        {
            lock (_thisLock)
            {
                if (_dataSource == null)
                {
                    _dataSource = CreateDataSource();
                    _dataSource.RegisterModel<Group>();
                    _dataSource.RegisterModel<ItemClass>();
                    _dataSource.RegisterModel<ItemInstance>();
                }
            }
            _persistenceLayer = await GetPersistenceLayerAsync();
        }

        public IDataSource GetDataSource()
        {
            return _dataSource;
        }

        private static IDataSource CreateDataSource()
        {
            return new BabyTouch.DataModel.DictionaryDataSource();
        }

        private static Task<Serialization.DirectoryJsonPersistence> GetPersistenceLayerAsync()
        {
            return Serialization.DirectoryJsonPersistence.Create("ms-appdata:///local/metadata/");
        }

        private static Task<Serialization.DirectoryJsonLoader> GetDefaultLoaderAsync()
        {
            return Serialization.DirectoryJsonLoader.Create("ms-appx:///DefaultContent/");
        }
    }
}
