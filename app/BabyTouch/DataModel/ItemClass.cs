﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace BabyTouch.DataModel
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ItemClass : CommonDataModel
    {
        public PersistentQuery<ItemInstance> Instances { get; set; }
        
        [JsonProperty]
        public string GroupId { get; set; }
        public Group Group {
            get
            {
                return DataSource.GetManager<Group>().GetById(GroupId);
            }
            set
            {
                GroupId = value.Id;
            }
        }

        [JsonProperty]
        public string MainInstanceId { get; set; }
        public ItemInstance MainInstance
        {
            get
            {
                return DataSource.GetManager<ItemInstance>().GetById(MainInstanceId);
            }
            set
            {
                MainInstanceId = value.Id;
            }
        }

        public ItemClass()
        {
            Instances = new PersistentQuery<ItemInstance>(this, e => e.ClassId == Id && e.IsEnabled == true);
        }
    }
}
