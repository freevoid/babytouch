﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BabyTouch.DataModel.Contracts;

namespace BabyTouch.DataModel
{
    public class PersistentQuery<T> where T : IDataModelProto, new()
    {
        private IDataModelProto _target;
        private Func<T, bool> _predicate;

        public PersistentQuery(IDataModelProto target, Func<T, bool> predicate)
        {
            _target = target;
            _predicate = predicate;
        }

        public ObservableCollection<T> All
        {
            get
            {
                return new ObservableCollection<T>(Execute());
            }
        }

        public IEnumerable<T> IterAll
        {
            get
            {
                return Execute();
            }
        }

        public IEnumerable<T> Execute()
        {
            var manager = _target.DataSource.GetManager<T>();
            return manager.GetAll().Where(_predicate);
        }
    }

}
