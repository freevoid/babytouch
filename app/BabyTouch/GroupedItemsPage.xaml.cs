﻿using BabyTouch.DataModel;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Grouped Items Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234231

namespace BabyTouch
{
    /// <summary>
    /// A page that displays a grouped collection of items.
    /// </summary>
    public sealed partial class GroupedItemsPage : BabyTouch.Common.LayoutAwarePage
    {
        private ItemClass currentClass;

        public GroupedItemsPage()
        {
            this.InitializeComponent();

            this.LayoutUpdated += GroupedItemsPage_LayoutUpdated;
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            // TODO: Create an appropriate data model for your problem domain to replace the sample data
            var dataGroups = DataSourceManager.DataSource.GetManager<Group>().GetAll();
            this.DefaultViewModel["Groups"] = dataGroups;

            if (pageState != null)
            {
                object lastGroupId;
                if (pageState.TryGetValue("lastGroupId", out lastGroupId))
                {
                    var lastGroupIdStr = lastGroupId as string;
                    var lastGroup = dataGroups.FirstOrDefault(g => g.Id == lastGroupIdStr);
                    if (lastGroup != null)
                    {
                        string lastClassId = pageState["lastClassId"] as string;
                        var firstClassInGroup = lastGroup.Classes.All.FirstOrDefault(c => c.Id == lastClassId);
                        if (firstClassInGroup != null)
                        {
                            this.currentClass = firstClassInGroup;
                        }
                    }
                }
            }

        }

        protected override void SaveState(Dictionary<string, object> pageState)
        {
            if (this.currentClass != null) {
                pageState["lastClassId"] = this.currentClass.Id;
                pageState["lastGroupId"] = this.currentClass.GroupId;
            }
        }

        /// <summary>
        /// Invoked when an item within a group is clicked.
        /// </summary>
        /// <param name="sender">The GridView (or ListView when the application is snapped)
        /// displaying the item clicked.</param>
        /// <param name="e">Event data that describes the item clicked.</param>
        void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Navigate to the appropriate destination page, configuring the new page
            // by passing required information as a navigation parameter
            this.currentClass = e.ClickedItem as ItemClass;
            var classId = this.currentClass.Id;
            this.Frame.Navigate(typeof(ItemClassShowPage), classId);
        }

        void GroupedItemsPage_LayoutUpdated(object sender, object e)
        {
            if (this.currentClass != null)
            {
                this.itemGridView.ScrollIntoView(this.currentClass, ScrollIntoViewAlignment.Leading);
                this.itemListView.ScrollIntoView(this.currentClass, ScrollIntoViewAlignment.Leading);
                this.currentClass = null;
            }
        }
    }
}
