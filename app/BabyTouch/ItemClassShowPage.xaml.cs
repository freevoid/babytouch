﻿using BabyTouch.Data;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using BabyTouch.DataModel;
using Windows.UI.Xaml.Media.Imaging;
using System.Collections.ObjectModel;

// The Item Detail Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234232

namespace BabyTouch
{
    /// <summary>
    /// A page that displays details for a single item within a group while allowing gestures to
    /// flip through other items belonging to the same group.
    /// </summary>
    public sealed partial class ItemClassShowPage : BabyTouch.Common.LayoutAwarePage
    {
        private bool isFirstHandling;
        private ItemInstance dummyBackInstance;

        public ItemClassShowPage()
        {
            this.InitializeComponent();

            this.dummyBackInstance = new ItemInstance();
            this.dummyBackInstance.ImageUri = "ms-appx:///Assets/transparent_pixel.png";
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            var itemClass = DataSourceManager.DataSource.GetManager<ItemClass>().GetById((String)navigationParameter);
            if (itemClass == null)
            {
                return;
            }
            this.DefaultViewModel["ItemClass"] = itemClass;

            var itemsList = itemClass.Instances.IterAll.ToList();
            itemsList.Shuffle();
            itemsList.Add(this.dummyBackInstance);
            itemsList.Insert(0, dummyBackInstance);
            //.Concat(items).Concat(new [] { dummyBackInstance }));

            this.isFirstHandling = true;
            this.DefaultViewModel["Items"] = itemsList;
            this.flipView.SelectedItem = itemsList[1];

        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values mustt conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        private void PlayCurrentSound()
        {
            var item = (ItemInstance)this.flipView.SelectedItem;
            var soundUri = item.SoundUri;

            this.sound.Stop();

            if (soundUri != null)
            {
                this.sound.Source = new Uri(soundUri);
                this.sound.Play();
            }
        }

        private void flipView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.flipView.SelectedItem != null)
            {
                if (this.isFirstHandling)
                {
                    this.isFirstHandling = false;
                    return;
                }

                var selected = flipView.SelectedItem as ItemInstance;

                if (string.IsNullOrEmpty(selected.ClassId))
                {
                    this.Frame.GoBack();
                    return;
                }

                PlayCurrentSound();
            }
        }
    }
}
