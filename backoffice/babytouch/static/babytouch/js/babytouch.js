$(document).ready(function () {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    $.ajaxSetup({
        crossDomain: false, // obviates need for sameOrigin test
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type)) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
});

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function init_crop(real_width, real_height, options) {
    var $crop_form = $(".babytouch__crop__form"),
        $crop_rectangle = $(".babytouch__crop__crop-rectangle");

    var image_real_width = real_width,
        image_real_height = real_height,
        image_view_width = 700,
        view_scale = image_view_width / image_real_width,
        min_real_width = options.min_real_width || 10,
        min_real_height = options.min_real_height || 10;

    var real_to_view = function (measure) { return measure * view_scale; };
    var view_to_real = function (measure) { return measure / view_scale; };
    var view_to_real_int = function (measure) { return Math.floor(view_to_real(measure)); };

    var image_view_height = real_to_view(image_real_height),
        min_view_width = real_to_view(min_real_width),
        min_view_height = real_to_view(min_real_height);

    input_names = options.input_names || { top: "top", left: "left", width: "width", height: "height" };

    $crop_rectangle.draggable({
        containment: "parent",
        stop: function (event, ui) {
            var new_position = ui.position;
            $crop_form.find("[name=" + input_names.left + "]").val(view_to_real_int(new_position.left));
            $crop_form.find("[name=" + input_names.top + "]").val(view_to_real_int(new_position.top));
        }
    });
    $crop_rectangle.resizable({
        aspectRatio: options.aspect_ratio || (16 / 9),
        minWidth: min_view_width,
        minHeight: min_view_height,
        containment: "parent",
        stop: function (event, ui) {
            var new_width = ui.element.outerWidth(),
                new_height = ui.element.outerHeight();

            $crop_form.find("[name=" + input_names.width + "]").val(view_to_real_int(new_width));
            $crop_form.find("[name=" + input_names.height + "]").val(view_to_real_int(new_height));
        }
    });

    var cur_width = real_to_view(options.crop_rectangle.width || min_real_width);
        cur_height = real_to_view(options.crop_rectangle.height || min_real_height);
        cur_top = real_to_view(options.crop_rectangle.top || 0),
        cur_left = real_to_view(options.crop_rectangle.left || 0);

    $crop_rectangle.width(cur_width);
    $crop_rectangle.height(cur_height);
    $crop_rectangle.css("top", cur_top);
    $crop_rectangle.css("left", cur_left);
    $crop_form.find("[name=" + input_names.top + "]").val(options.crop_rectangle.top || 0);
    $crop_form.find("[name=" + input_names.left + "]").val(options.crop_rectangle.left || 0);
    $crop_form.find("[name=" + input_names.width + "]").val(options.crop_rectangle.width || min_real_width);
    $crop_form.find("[name=" + input_names.height + "]").val(options.crop_rectangle.height || min_real_height);
}

function init_itemclass_list($itemclass_objects)
{
    $(".babytouch__itemclass__controls__toggle-enabled", $itemclass_objects).click(function () {
        var $this = $(this);

        var item_class = $this.closest(".babytouch__itemclass__item");

        var is_enabled = !item_class.hasClass("babytouch__itemclass_disabled");

        var class_id = item_class.attr("data-class-id");

        var url = "/api/itemclass/" + class_id + (is_enabled ? "/disable/" : "/enable/");

        $this.hide();
        $.post(url, function (data) {
            item_class.toggleClass("babytouch__itemclass_disabled");
        }).always(function () {
            $this.show();
        });
    });
}

function init_itemclass($itemclass_objects)
{
    $(".babytouch__itemclass__instance__controls__toggle-enabled", $itemclass_objects).click(function () {
        var item_instance = $(this).closest(".babytouch__itemclass__instance");

        var is_enabled = !item_instance.hasClass("babytouch__itemclass__instance_disabled");

        var id = item_instance.attr("data-instance-id");

        var url = "/api/iteminstance/" + id + (is_enabled ? "/disable/" : "/enable/");

        $.post(url, function (data) {
            item_instance.toggleClass("babytouch__itemclass__instance_disabled");
        });
    });

    $(".babytouch__itemclass__instance__controls__set-as-main", $itemclass_objects).click(function () {
        var main_class_name = "babytouch__itemclass__instance_main";
        var item_class = $(this).closest(".babytouch__itemclass");
        var item_instance = $(this).closest(".babytouch__itemclass__instance");

        var is_main = item_instance.hasClass(main_class_name);

        if (is_main)
        {
            return;
        }

        var id = item_instance.attr("data-instance-id");

        var url = "/api/iteminstance/" + id + "/set_as_main_instance/";

        $.post(url, function (data) {
            $(".babytouch__itemclass__instance", item_class).removeClass(main_class_name);
            item_instance.toggleClass(main_class_name);
        });
    });

    var available_sounds = $(".babytouch__itemclass__sounds .babytouch__sound", $itemclass_objects);

    available_sounds.draggable({
        revert: "invalid",
        delay: 300,
        helper: function () {
            return $(this).find(".babytouch__sound__filename").clone();
        },
        handle: ".babytouch__sound__filename",
    });

    var thumbnails = $(".babytouch__iteminstance__thumbnail", $itemclass_objects);

    thumbnails.droppable({
        tolerance: "pointer",
        drop: function (event, ui) {
            var dropped_sound = ui.draggable,//.closest(".babytouch__sound"),
                dropped_sound_id = dropped_sound.attr("data-sound-id"),
                thumbnail = $(this),
                existing_sound = thumbnail.nextAll(".babytouch__sound"),
                instance_id = thumbnail.attr("data-instance-id");

            if (existing_sound.length != 0)
            {
                var existing_sound_id = existing_sound.attr("data-sound-id");
                if (existing_sound_id === dropped_sound_id)
                {
                    return;
                }
            }

            $.post("/api/iteminstance/" + instance_id + "/set_sound/",
                {
                    sound_id: dropped_sound_id
                },
                function (data) {
                    existing_sound.remove();
                    dropped_sound.clone().insertAfter(thumbnail);
                });
        }
    });
}
