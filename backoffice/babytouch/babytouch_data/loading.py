import os
import sys
import glob
import json

from django.core.files.images import ImageFile
from .models import ItemInstance


def load_babytouch_json(json_filename, model_cls):
    with open(json_filename) as fp:
        content = fp.read()
    if content.startswith('\xef\xbb\xbf'):
        content = content[3:]
    objs = json.loads(content)

    for dict_obj in objs:
        dict_obj = model_cls.from_dict(dict_obj)
        dict_obj.save()


def _extract_copyright_from_directory_path(directory_path):
    directory_path = directory_path.rstrip(os.path.sep)
    base_path, directory_name = os.path.split(directory_path)
    return directory_name


def bootstrap_instances_from_images(directory_path, copyright=None, stdout=sys.stdout, pattern='*.jpg'):

    if copyright is None:
        copyright = _extract_copyright_from_directory_path(directory_path)

    filenames = glob.glob1(directory_path, pattern)
    nfiles = len(filenames)

    for i, filename in enumerate(filenames):
        if ItemInstance.objects.filter(id=filename).exists():
            continue

        stdout.write('Loading file: %5d / %d' % (i + 1, nfiles))
        stdout.flush()
        full_path = os.path.join(directory_path, filename)
        with open(full_path, 'rb') as fp:
            image_file = ImageFile(fp)
            instance = ItemInstance(id=filename, image=image_file, copyright=copyright)
            stdout.write('%s %s' % (instance.id, copyright))
            instance.save()
