import os

from django.db import models
from django.core.files import File


class CommonDataModel(models.Model):

    class Meta:
        abstract = True

    appx_prefix = 'ms-appx:///DefaultContent/'

    id = models.CharField(max_length=128, primary_key=True, unique=True)
    title = models.CharField(max_length=256, default='')
    is_enabled = models.BooleanField(default=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def to_dict(self):
        return {
            'Id': self.id,
            'Title': self.title,
            'IsEnabled': self.is_enabled,
        }

    @staticmethod
    def remove_appx_prefix_from_uri(uri):
        if uri.startswith(CommonDataModel.appx_prefix):
            return uri[len(CommonDataModel.appx_prefix):]
        else:
            return uri

    @staticmethod
    def add_appx_prefix_to_uri(uri):
        return CommonDataModel.appx_prefix + uri

    def __unicode__(self):
        return '[%s] %s' % (self.id, self.title)


class Group(CommonDataModel):

    @classmethod
    def from_dict(cls, data):
        return Group(
            id=data['Id'],
            title=data['Title'],
            is_enabled=data.get('IsEnabled', True),
        )


class ItemClass(CommonDataModel):
    group = models.ForeignKey(Group)
    main_instance = models.OneToOneField('ItemInstance', null=True,
        related_name='class_where_iam_main')

    def to_dict(self):
        base = super(ItemClass, self).to_dict()
        base.update({
            'GroupId': self.group_id,
            'MainInstanceId': self.main_instance_id,
        })
        return base

    @classmethod
    def from_dict(cls, data):
        return ItemClass(
            id=data['Id'],
            title=data['Title'],
            is_enabled=data.get('IsEnabled', True),
            group_id=data['GroupId'],
            main_instance_id=data['MainInstanceId'],
        )

    def add_sound_from_file(self, filepath):
        sound = ItemClassSound(item_class=self)
        with open(filepath, 'rb') as fp:
            sound.sound = File(fp)
            sound.save()

        return sound


class ItemClassSound(models.Model):
    item_class = models.ForeignKey(ItemClass, related_name='sounds')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    sound = models.FileField(upload_to='Sounds')

    def filename(self):
        return os.path.basename(self.sound.path)

    def __unicode__(self):
        return '[%s] %s' % (self.item_class_id, self.filename())


class ItemInstance(CommonDataModel):
    item_class = models.ForeignKey(ItemClass, null=True, related_name='instances')
    sound = models.ForeignKey(ItemClassSound, null=True, related_name='instances')
    copyright = models.CharField(max_length=128, default='')

    # photo (original size)
    image = models.ImageField(upload_to='Images/unsorted', null=True, height_field='image_height', width_field='image_width')
    image_height = models.IntegerField(default=0)
    image_width = models.IntegerField(default=0)

    # thumbnail
    image_thumbnail = models.ImageField(upload_to='Thumbnails', null=True)
    thumbnail_crop_top = models.IntegerField(default=0)
    thumbnail_crop_left = models.IntegerField(default=0)
    thumbnail_crop_size = models.IntegerField(default=0)

    # crop rectangle info
    crop_rectangle_top = models.IntegerField(default=0)
    crop_rectangle_left = models.IntegerField(default=0)
    crop_rectangle_width = models.IntegerField(default=0)
    crop_rectangle_height = models.IntegerField(default=0)

    THUMBNAIL_WIDTH = 250

    def to_dict(self):
        base = super(ItemInstance, self).to_dict()
        base.update({
            'ClassId': self.item_class_id,
            'Copyright': self.copyright,
            'SoundUri': self.add_appx_prefix_to_uri(self.sound.sound.name) if self.sound else None,
            'ImageUri': self.add_appx_prefix_to_uri(self.image.name) if self.image else None,
            'ThumbnailUri': self.add_appx_prefix_to_uri(self.image_thumbnail.name) if self.image_thumbnail else None
        })
        return base

    @classmethod
    def from_dict(cls, data):
        return ItemInstance(
            id=data['Id'],
            title=data.get('Title', ''),
            is_enabled=data.get('IsEnabled', True),
            sound=ItemClassSound(item_class_id=data['ClassId'], sound=cls.remove_appx_prefix_from_uri(data.get('SoundUri', ''))),
            image=cls.remove_appx_prefix_from_uri(data.get('ImageUri', '')),
            item_class_id=data['ClassId'],
            copyright=data.get('Copyright', ''),
        )


class IMAGE_SCALE:
    SCALE_80 = 80
    SCALE_100 = 100
    SCALE_140 = 140
    SCALE_180 = 180

    SCALE_100_SIZE = (1366, 768)
    SCALE_140_SIZE = (1920, 1080)
    SCALE_180_SIZE = (2560, 1440)

    scales = (SCALE_80, SCALE_100, SCALE_140, SCALE_180)
    choices = [
        (SCALE_80, '80%'),
        (SCALE_100, '100%'),
        (SCALE_140, '140%'),
        (SCALE_180, '180%'),
    ]
    asset_sizes = {
        SCALE_100: SCALE_100_SIZE,
        SCALE_140: SCALE_140_SIZE,
        SCALE_180: SCALE_180_SIZE
    }


class ItemImage(models.Model):
    instance = models.ForeignKey(ItemInstance)
    image = models.ImageField(upload_to='Images/scaled', null=True)
    scale = models.IntegerField(choices=IMAGE_SCALE.choices)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
