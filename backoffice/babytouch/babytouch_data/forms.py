from django import forms
from .models import ItemInstance, ItemClass, ItemClassSound

class InstanceToClassMappingForm(forms.Form):
    item_class = forms.ModelChoiceField(ItemClass.objects.all())
    instances = forms.ModelMultipleChoiceField(ItemInstance.objects.all())


class CropForm(forms.ModelForm):

    class Meta:
        model = ItemInstance
        fields = ('crop_rectangle_top', 'crop_rectangle_left', 'crop_rectangle_width', 'crop_rectangle_height')


class ThumbnailCropForm(forms.ModelForm):

    class Meta:
        model = ItemInstance
        fields = ('thumbnail_crop_top', 'thumbnail_crop_left', 'thumbnail_crop_size')


class AddSoundForm(forms.ModelForm):

    class Meta:
        model = ItemClassSound
        fields = ('sound', 'item_class')
        widgets = {
            'item_class': forms.HiddenInput,
        }
