from django.contrib import admin
from django.db.models import F

from babytouch.babytouch_data.models import Group, ItemClass, ItemInstance

class CommonDataAdmin(admin.ModelAdmin):

    actions = ('enable', 'disable')
    search_fields = ('id', 'title')
    list_filter = ('is_enabled',)
    list_select_related = True

    def enable(self, request, qs):
        nupdated = qs.update(is_enabled=True)
        self.message_user(request, '%d entities enabled' % nupdated)

    def disable(self, request, qs):
        nupdated = qs.update(is_enabled=False)
        self.message_user(request, '%d entities disabled' % nupdated)


class GroupAdmin(CommonDataAdmin):
    list_display = ('id', 'title', 'is_enabled')


class ItemClassAdmin(CommonDataAdmin):
    list_display = ('id', 'title', 'group', 'main_instance', 'is_enabled')
    actions = CommonDataAdmin.actions + ('fix_main_instance',)

    def fix_main_instance(self, request, qs):
        for cls in ItemClass.objects.exclude(instances__isnull=True).exclude(main_instance__item_class__pk=F('id')):
            cls.main_instance = cls.instances.all()[0]
            cls.save()


class ItemInstanceAdmin(CommonDataAdmin):
    list_display = ('id', 'item_class', 'is_enabled', 'image', 'sound', 'copyright')
    search_fields = CommonDataAdmin.search_fields + ('item_class__title',)


admin.site.register(Group, GroupAdmin)
admin.site.register(ItemClass, ItemClassAdmin)
admin.site.register(ItemInstance, ItemInstanceAdmin, list_select_related=True)
