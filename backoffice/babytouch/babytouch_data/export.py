import itertools
import os
import shutil
import sys

from django.utils import simplejson as json
from django.db.models import F
from django.conf import settings

from .models import CommonDataModel, ItemImage, IMAGE_SCALE


def remove_scale_qualifier(image_name):
    base_with_qualifier, ext = os.path.splitext(image_name)
    base, qualifier = os.path.splitext(base_with_qualifier)
    if qualifier.startswith('.scale-'):
        return base + ext
    return image_name


def get_publishable_images(with_sounds_only=False, scales=None):
    result = (ItemImage.objects
        .select_related('instance__item_class__group')
        .select_related('instance__sound')
        .filter(instance__is_enabled=True,
                instance__item_class__is_enabled=True,
                instance__item_class__main_instance__item_class=F('instance__item_class'),
                instance__item_class__main_instance__is_enabled=True,
                instance__item_class__main_instance__itemimage__isnull=False,
                instance__item_class__main_instance__image_thumbnail__isnull=False,
                instance__item_class__group__is_enabled=True)
        )

    if with_sounds_only:
        result = result.filter(instance__item_class__main_instance__sound__isnull=False)

    if scales is not None:
        result = result.filter(scale__in=scales)

    return result


def export_to_babytouch_format(with_sounds_only=False, scales=None):
    images = get_publishable_images(with_sounds_only=with_sounds_only, scales=scales)
    images = images.order_by('instance__item_class__group',
                             'instance__item_class',
                             'instance',
                             'scale')
    grouped_by_instance = itertools.groupby(images, lambda im: im.instance)

    classes = []
    groups = []
    instances = []
    image_paths = []
    sound_paths = []
    current_class = None
    current_group = None

    for instance, images in grouped_by_instance:
        as_dict = instance.to_dict()
        if current_class != instance.item_class:
            classes.append(instance.item_class.to_dict())
            current_class = instance.item_class
            if current_group != current_class.group:
                groups.append(current_class.group.to_dict())
                current_group = current_class.group

        images = list(images)
        scaled_image = images[0].image
        name_wo_qualifier = remove_scale_qualifier(scaled_image.name)
        as_dict['ImageUri'] = CommonDataModel.add_appx_prefix_to_uri(name_wo_qualifier)

        for item_image in images:
            image_paths.append((item_image.image.path, item_image.image.name))

        image_paths.append((instance.image_thumbnail.path, instance.image_thumbnail.name))

        instances.append(as_dict)
        if instance.sound:
            sound_paths.append((instance.sound.sound.path, instance.sound.sound.name))

    return {
        'image_paths': image_paths,
        'sound_paths': sound_paths,
        'instances': instances,
        'classes': classes,
        'groups': groups,
    }


def create_babytouch_resources(base_folder=None, copy_content=True, with_sounds_only=False, scales=(IMAGE_SCALE.SCALE_100,), stdout=sys.stdout):

    if base_folder is None:
        base_folder = os.path.join(settings.MEDIA_ROOT, 'export')
    exported_metadata = export_to_babytouch_format(with_sounds_only=with_sounds_only,
                                                   scales=scales)

    if not os.path.exists(base_folder):
        os.makedirs(base_folder)

    with open(os.path.join(base_folder, 'Group.json'), 'w') as fp:
        json.dump(exported_metadata['groups'], fp)

    with open(os.path.join(base_folder, 'ItemClass.json'), 'w') as fp:
        json.dump(exported_metadata['classes'], fp)

    with open(os.path.join(base_folder, 'ItemInstance.json'), 'w') as fp:
        json.dump(exported_metadata['instances'], fp)

    if copy_content:
        stdout.write('Copying content: %d images, %d sounds' % (len(exported_metadata['image_paths']), len(exported_metadata['sound_paths'])))
        copy_many(exported_metadata['image_paths'], base_folder)
        copy_many(exported_metadata['sound_paths'], base_folder)

def copy_many(paths_to_copy, base_folder):
    for from_path, relative_name in paths_to_copy:
        to_path = os.path.join(base_folder, relative_name)
        to_dir = os.path.dirname(to_path)

        if not os.path.exists(to_dir):
            os.makedirs(to_dir)

        shutil.copy(from_path, to_path)
