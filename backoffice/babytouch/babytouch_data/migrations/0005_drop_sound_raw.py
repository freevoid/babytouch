# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'ItemInstance.sound_raw'
        db.delete_column(u'babytouch_data_iteminstance', 'sound_raw')


    def backwards(self, orm):
        # Adding field 'ItemInstance.sound_raw'
        db.add_column(u'babytouch_data_iteminstance', 'sound_raw',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True),
                      keep_default=False)


    models = {
        u'babytouch_data.group': {
            'Meta': {'object_name': 'Group'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128', 'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'babytouch_data.itemclass': {
            'Meta': {'object_name': 'ItemClass'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['babytouch_data.Group']"}),
            'id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128', 'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'main_instance': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['babytouch_data.ItemInstance']", 'null': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'babytouch_data.itemclasssound': {
            'Meta': {'object_name': 'ItemClassSound'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_class': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sounds'", 'to': u"orm['babytouch_data.ItemClass']"}),
            'sound': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'babytouch_data.itemimage': {
            'Meta': {'object_name': 'ItemImage'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'instance': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['babytouch_data.ItemInstance']"}),
            'scale': ('django.db.models.fields.IntegerField', [], {}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'babytouch_data.iteminstance': {
            'Meta': {'object_name': 'ItemInstance'},
            'copyright': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'crop_rectangle_height': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'crop_rectangle_left': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'crop_rectangle_top': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'crop_rectangle_width': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128', 'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'image_height': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'image_thumbnail': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'image_width': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'item_class': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'instances'", 'null': 'True', 'to': u"orm['babytouch_data.ItemClass']"}),
            'sound': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'instances'", 'null': 'True', 'to': u"orm['babytouch_data.ItemClassSound']"}),
            'thumbnail_crop_left': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'thumbnail_crop_size': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'thumbnail_crop_top': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['babytouch_data']