# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Group'
        db.create_table(u'babytouch_data_group', (
            ('id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=128, primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(default='', max_length=256)),
            ('is_enabled', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'babytouch_data', ['Group'])

        # Adding model 'ItemClass'
        db.create_table(u'babytouch_data_itemclass', (
            ('id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=128, primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(default='', max_length=256)),
            ('is_enabled', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['babytouch_data.Group'])),
            ('main_instance', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['babytouch_data.ItemInstance'], null=True)),
        ))
        db.send_create_signal(u'babytouch_data', ['ItemClass'])

        # Adding model 'ItemInstance'
        db.create_table(u'babytouch_data_iteminstance', (
            ('id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=128, primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(default='', max_length=256)),
            ('is_enabled', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('item_class', self.gf('django.db.models.fields.related.ForeignKey')(related_name='instances', null=True, to=orm['babytouch_data.ItemClass'])),
            ('sound', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True)),
            ('copyright', self.gf('django.db.models.fields.CharField')(default='', max_length=128)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True)),
            ('image_height', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('image_width', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('image_thumbnail', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True)),
            ('thumbnail_crop_top', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('thumbnail_crop_left', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('thumbnail_crop_size', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('crop_rectangle_top', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('crop_rectangle_left', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('crop_rectangle_width', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('crop_rectangle_height', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'babytouch_data', ['ItemInstance'])

        # Adding model 'ItemImage'
        db.create_table(u'babytouch_data_itemimage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('instance', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['babytouch_data.ItemInstance'])),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True)),
            ('scale', self.gf('django.db.models.fields.IntegerField')()),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'babytouch_data', ['ItemImage'])


    def backwards(self, orm):
        # Deleting model 'Group'
        db.delete_table(u'babytouch_data_group')

        # Deleting model 'ItemClass'
        db.delete_table(u'babytouch_data_itemclass')

        # Deleting model 'ItemInstance'
        db.delete_table(u'babytouch_data_iteminstance')

        # Deleting model 'ItemImage'
        db.delete_table(u'babytouch_data_itemimage')


    models = {
        u'babytouch_data.group': {
            'Meta': {'object_name': 'Group'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128', 'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'babytouch_data.itemclass': {
            'Meta': {'object_name': 'ItemClass'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['babytouch_data.Group']"}),
            'id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128', 'primary_key': 'True'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'main_instance': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['babytouch_data.ItemInstance']", 'null': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'babytouch_data.itemimage': {
            'Meta': {'object_name': 'ItemImage'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'instance': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['babytouch_data.ItemInstance']"}),
            'scale': ('django.db.models.fields.IntegerField', [], {}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'babytouch_data.iteminstance': {
            'Meta': {'object_name': 'ItemInstance'},
            'copyright': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '128'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'crop_rectangle_height': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'crop_rectangle_left': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'crop_rectangle_top': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'crop_rectangle_width': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128', 'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'image_height': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'image_thumbnail': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'image_width': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'is_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'item_class': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'instances'", 'null': 'True', 'to': u"orm['babytouch_data.ItemClass']"}),
            'sound': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True'}),
            'thumbnail_crop_left': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'thumbnail_crop_size': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'thumbnail_crop_top': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '256'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['babytouch_data']
