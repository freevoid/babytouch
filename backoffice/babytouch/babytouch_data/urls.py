from django.conf.urls import patterns, include, url
from django.conf import settings


urlpatterns = patterns('babytouch.babytouch_data.views',
    url(r'^sort/$', 'instance_sort', name='instance_sort'),
    url(r'^itemclass/$', 'itemclass_manage', name='itemclass_manage'),
    url(r'^itemclass/(?P<pk>[0-9A-Za-z._-]+)/$', 'itemclass_manage_single', name='itemclass_manage_single'),
    url(r'^crop/$', 'set_cropping_rectangle', name='set_cropping_rectangle'),
    url(r'^crop/(?P<instance_pk>[0-9A-Za-z._-]+)/$', 'set_cropping_rectangle', name='set_cropping_rectangle'),
    url(r'^thumbcrop/$', 'set_thumbnail_cropping_rectangle', name='set_thumbnail_cropping_rectangle'),
    url(r'^thumbcrop/(?P<instance_pk>[0-9A-Za-z._-]+)/$', 'set_thumbnail_cropping_rectangle', name='set_thumbnail_cropping_rectangle'),

    url(r'^itemclasssound/add/$', 'itemclasssound_add', name='itemclasssound_add'),
)

urlpatterns += patterns('babytouch.babytouch_data.api',
    url(r'^api/iteminstance/(?P<pk>[0-9A-Za-z._-]+)/enable/$', 'iteminstance_enable'),
    url(r'^api/iteminstance/(?P<pk>[0-9A-Za-z._-]+)/disable/$', 'iteminstance_disable'),
    url(r'^api/iteminstance/(?P<pk>[0-9A-Za-z._-]+)/set_as_main_instance/$', 'iteminstance_set_as_main_instance'),
    url(r'^api/iteminstance/(?P<pk>[0-9A-Za-z._-]+)/set_sound/$', 'iteminstance_set_sound'),
    url(r'^api/itemclass/(?P<pk>[0-9A-Za-z._-]+)/enable/$', 'itemclass_enable'),
    url(r'^api/itemclass/(?P<pk>[0-9A-Za-z._-]+)/disable/$', 'itemclass_disable'),
)
