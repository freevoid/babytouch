from optparse import make_option

from django.core.management.base import BaseCommand

from babytouch.babytouch_data.loading import bootstrap_instances_from_images


class Command(BaseCommand):

    args = '<directory>'

    option_list = BaseCommand.option_list + (
        make_option('--copyright',
            action='store',
            dest='copyright',
            default=None,
            help='Copyright string to save along with image'),
        )

    def handle(self, directory, **kwargs):
        copyright = kwargs.get('copyright')
        bootstrap_instances_from_images(directory, copyright=copyright, stdout=self.stdout)
