from django.core.management.base import BaseCommand

from babytouch.babytouch_data import models
from babytouch.babytouch_data.loading import load_babytouch_json

class Command(BaseCommand):

    args = '<filename> <model_name>'

    def handle(self, filename, model_name, **kwargs):
        model_cls = getattr(models, model_name)
        load_babytouch_json(filename, model_cls)
