from __future__ import unicode_literals
from optparse import make_option
import os
import glob
import itertools

from django.utils.encoding import smart_unicode
from django.core.management.base import BaseCommand

from babytouch.babytouch_data.models import ItemClass, ItemClassSound


class Command(BaseCommand):
    args = '<folder> <classid>'

    option_list = BaseCommand.option_list + (
        make_option('-f', '--filter',
            action='store',
            dest='filter',
            default=None,
            help='Substring to filter filenames'),
        make_option('-d', '--dry-run',
            action='store_true',
            dest='dry_run',
            default=False,
            help='Do not actually save anything in DB')
        )

    def handle(self, *args, **kwargs):
        folder, class_id = args
        folder = smart_unicode(folder)
        filter_substring = smart_unicode(kwargs['filter'])

        try:
            item_class = ItemClass.objects.get(pk=class_id)
        except ItemClass.DoesNotExist:
            self.stdout.write('Item class with id "%s" not found' % class_id)
            return

        if os.path.isdir(folder):
            patterns = [
                os.path.join(folder, '*.mp3'),
                os.path.join(folder, '*.wav')
            ]

            matching_filenames = [
                smart_unicode(os.path.basename(path)) for path in
                    filter(
                        lambda filename: filter_substring.lower() in filename.lower()
                                        if filter_substring else True,
                        itertools.chain(*map(glob.iglob, patterns)))]

        elif os.path.isfile(folder):
            matching_filenames = [os.path.basename(folder)]
            folder = os.path.dirname(folder)

        else:
            self.stdout.write('Directory or file not found: "%s"' % folder)
            return


        existing_sounds = ItemClassSound.objects.values_list('sound', flat=True);
        existing_filenames = map(os.path.basename, existing_sounds)

        matching_filenames_to_compare_index = dict((filename.replace(' ', '_'), filename) for (i, filename) in enumerate(matching_filenames))
        matching_filenames_to_compare = set(matching_filenames_to_compare_index.keys())


        non_existing_sounds_slugs = list(matching_filenames_to_compare.difference(existing_filenames))

        non_existing_sounds = [
            matching_filenames_to_compare_index[slug]
            for slug in non_existing_sounds_slugs
        ]

        non_existing_sounds.sort()

        self.stdout.write('Found %d new sounds out of %d in the folder' % (len(non_existing_sounds), len(matching_filenames)))

        if not non_existing_sounds:
            return

        for filename in non_existing_sounds:
            fullpath = os.path.join(folder, filename)
            if not kwargs['dry_run']:
                item_class.add_sound_from_file(fullpath)
            self.stdout.write('"%s" added.' % fullpath)
