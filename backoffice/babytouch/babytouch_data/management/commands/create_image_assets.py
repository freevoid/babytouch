from optparse import make_option

from django.core.management.base import BaseCommand
from django.db.models import Q, F

from babytouch.babytouch_data.models import ItemInstance
from babytouch.babytouch_data.images import create_image_assets


class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('--force',
            action='store_true',
            dest='force',
            default=None,
            help='Force rescaling ALL cropped instances'),
        )

    def handle(self, *args, **kwargs):
        # pick only images with cropping rectangle and without any scale assets
        instances = ItemInstance.objects.filter(crop_rectangle_width__gt=0)
        if not kwargs['force']:
            instances = instances.filter(Q(itemimage__isnull=True) | Q(itemimage__updated_at__lt=F('updated_at'))).distinct()

        max_exceptions = 5
        ninstances = len(instances)
        nexceptions = 0

        self.stdout.write('Starting to crop-and-scale images. Images to process: %d' % ninstances)

        for i, x in enumerate(instances):
            try:
                create_image_assets(x)
            except KeyboardInterrupt:
                self.stdout.write('Interrupted, exiting..')
                break
            except Exception as e:
                self.stdout.write('Exception on image: %4d / %d' % (i + 1, ninstances))
                self.stdout.write(unicode(e))
                nexceptions += 1
                if nexceptions >= max_exceptions:
                    self.stdout.write('Maximum of %d exceptions in-a-row reached. Aborting.' % max_exceptions)
                    break
            else:
                nexceptions = 0
                self.stdout.write('Processed images: %4d / %d' % (i + 1, ninstances))
            self.stdout.flush()
