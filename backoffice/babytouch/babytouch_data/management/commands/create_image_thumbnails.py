from optparse import make_option
import os
import calendar

from django.core.management.base import BaseCommand

from babytouch.babytouch_data.models import ItemInstance
from babytouch.babytouch_data.images import make_thumbnail


class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('-f', '--force',
            action='store_true',
            dest='force',
            default=False,
            help='Force rethumbnailing all cropped instances'),
        make_option('-n', '--noskip',
            action='store_true',
            dest='noskip',
            default=False,
            help='By default we will skip creation if thumbnail already exists and newer than update time. Set this flag together with force to REALLY recreate all thumbnails.'),
        )

    def handle(self, *args, **kwargs):
        is_force = kwargs['force']
        skip_enabled = not kwargs['noskip']

        # pick only images with cropping rectangle
        instances = ItemInstance.objects.filter(thumbnail_crop_size__gt=0)

        if not is_force:
            instances = instances.filter(image_thumbnail__isnull=True)

        max_exceptions = 5
        ninstances = len(instances)
        nexceptions = 0

        self.stdout.write('Starting to thumbnail images. Images to process: %d' % ninstances)

        nskipped = 0

        for i, x in enumerate(instances):
            if skip_enabled and self.is_up_to_date(x):
                nskipped += 1
                continue

            try:
                make_thumbnail(x, is_force=is_force)
            except KeyboardInterrupt:
                self.stdout.write('Interrupted, exiting..')
                if x.image_thumbnail and x.image_thumbnail.path:
                    self.stdout.write('You may want to delete leftovers: "%s"' % x.image_thumbnail.path)
                break
            except Exception as e:
                self.stdout.write('Exception on image: %4d / %d' % (i + 1, ninstances))
                self.stdout.write(unicode(e))
                if x.image_thumbnail and x.image_thumbnail.path:
                    self.stdout.write('You may want to delete leftovers: "%s"' % x.image_thumbnail.path)
                nexceptions += 1
                if nexceptions >= max_exceptions:
                    self.stdout.write('Maximum of %d exceptions in-a-row reached. Aborting.' % max_exceptions)
                    break
            else:
                nexceptions = 0
                self.stdout.write('Processed images: %4d / %d' % (i + 1, ninstances))
            self.stdout.flush()

        self.stdout.write('Skipped %d images, really processed: %d' % (nskipped, ninstances - nskipped))

    def is_up_to_date(self, instance):
        if not instance.image_thumbnail or not instance.image_thumbnail.path or not os.path.isfile(instance.image_thumbnail.path):
            return False

        stat = os.stat(instance.image_thumbnail.path)

        instance_updated_at_timestamp = calendar.timegm(instance.updated_at.utctimetuple())
        thumbnail_modified_date = stat.st_mtime

        return instance_updated_at_timestamp < thumbnail_modified_date
