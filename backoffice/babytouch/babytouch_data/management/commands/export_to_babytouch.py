import os
import datetime
import glob
from optparse import make_option

from django.core.management.base import BaseCommand
from django.conf import settings

from babytouch.babytouch_data.models import IMAGE_SCALE
from babytouch.babytouch_data.export import create_babytouch_resources


class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('-r', '--root',
            action='store',
            dest='root',
            default=None,
            help='Root directory to export content and metadata'),
        make_option('-n', '--no-sounds',
            action='store_true',
            dest='include_no_sounds',
            default=False,
            help='If specified, publish will include classes without sound available for main instance.'),
        make_option('-a', '--assets',
            action='store',
            dest='assets',
            default=None,
            help='Comma-separated list of scales to include (by default - "100"). Scale is one of {100, 140, 180}.'),
        make_option('--no-content',
            action='store_true',
            dest='dont_copy_content',
            default=False,
            help="Specify this option if you don't want to copy all images/sounds (only metadata will be exported)"),
        )

    def handle(self, *args, **kwargs):
        root = kwargs['root']
        with_sounds_only = not kwargs['include_no_sounds']
        dont_copy_content = kwargs['dont_copy_content']
        scales_str = kwargs['assets']
        if scales_str is None:
            scales = (IMAGE_SCALE.SCALE_100,)
        else:
            scales = map(int, scales_str.split(','))

        if root is None:
            basedir = os.path.join(settings.PROJECT_PATH, 'export')
            now = datetime.datetime.now()

            dirname_attempt = now.strftime("%Y_%m_%d.0")
            root = os.path.join(basedir, dirname_attempt)
            if os.path.isdir(root):
                first_part = dirname_attempt[:-2]
                last_exported_dirname = max(glob.glob1(basedir, first_part + ".*"))
                iteration = int(last_exported_dirname.rsplit('.', 1)[1])
                iteration += 1
                root = os.path.join(basedir, first_part + ".%d" % iteration)

        self.stdout.write('Creating babytouch app resources at "%s"..' % root)
        create_babytouch_resources(root, copy_content=not dont_copy_content, with_sounds_only=with_sounds_only, scales=scales, stdout=self.stdout)
