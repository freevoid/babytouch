from optparse import make_option

from django.core.management.base import BaseCommand
from django.conf import settings

from babytouch.babytouch_data.images import optimize_recursive


class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('--root',
            action='store',
            dest='root',
            default=settings.MEDIA_ROOT,
            help='Root directory to find images for optimization'),
        )

    def handle(self, *args, **kwargs):
        if not optimize_recursive(kwargs['root'], stdout=self.stdout):
            self.stderr('Errors occured. Optimization aborted.')
