from django import template

register = template.Library()

MEDIA_TYPES_MAPPING = {
    'mp3': 'audio/mpeg',
    'ogg': 'audio/ogg',
    'wav': 'audio/wav',
}

@register.simple_tag
def get_media_type(url):
    extension = url.rsplit('.', 1)[1]
    return MEDIA_TYPES_MAPPING.get(extension.lower())
