from django.shortcuts import render, redirect, get_object_or_404
from django.db.models import Count, F
from django.views.decorators.http import require_http_methods
from django.http import HttpResponse

from .models import ItemInstance, ItemClass, IMAGE_SCALE
from .forms import InstanceToClassMappingForm, CropForm, ThumbnailCropForm, AddSoundForm


def instance_sort(request):
    if request.method == 'POST':
        form = InstanceToClassMappingForm(request.POST)
        if form.is_valid():
            item_class = form.cleaned_data['item_class']
            instances = form.cleaned_data['instances']
            instances.filter(item_class__isnull=True).update(item_class=item_class)
        else:
            print 'Form validation error occured', form.errors

    classes = ItemClass.objects.all().select_related().order_by('group__title', 'title')
    rarest_classes = (ItemClass.objects.all()
        .annotate(count_instances=Count('instances'))
        .order_by('count_instances')[:10])
    fullest_classes = (ItemClass.objects.all()
        .annotate(count_instances=Count('instances'))
        .order_by('-count_instances')[:10])
    instances = ItemInstance.objects.filter(item_class__isnull=True)
    context = {
        'classes': classes,
        'rarest_classes': rarest_classes,
        'fullest_classes': fullest_classes,
        'instances': instances,
    }
    return render(request, 'babytouch/sort.html', context)


def _set_cropping_rectangle(request, instance_pk=None, form_cls=None, initial_queryset=None, urlname=None, template_name=None):

    instances_without_cropping = initial_queryset
    if instance_pk is None:
        instances_pair = instances_without_cropping.order_by('pk')[:2]
        if len(instances_pair) == 2:
            instance, next_instance = instances_pair
        else:
            assert len(instances_pair) == 1
            instance, next_instance = instances_pair[0], None

    else:
        instance = ItemInstance.objects.get(pk=instance_pk)
        try:
            next_instance = instances_without_cropping.filter(pk__gt=instance_pk).order_by('pk')[0]
        except IndexError:
            next_instance = None

    if request.method == 'POST':
        form = form_cls(request.POST, instance=instance)
        if form.is_valid():
            form.save()
            if next_instance is not None:
                return redirect(urlname, instance_pk=next_instance.pk)
    else:
        form = form_cls(instance=instance)

    context = {
        'instance': instance,
        'next_instance': next_instance,
        'crop_form': form,
        'instances_without_cropping_count': instances_without_cropping.count(),
        'IMAGE_SCALE': IMAGE_SCALE,
        'crop_urlname': urlname,
    }
    return render(request, template_name, context)


def set_cropping_rectangle(request, instance_pk=None):
    return _set_cropping_rectangle(request,
        instance_pk=instance_pk,
        form_cls=CropForm,
        initial_queryset=ItemInstance.objects.filter(crop_rectangle_width=0),
        urlname='babytouch:set_cropping_rectangle',
        template_name='babytouch/set_cropping_rectangle.html',
    )


def set_thumbnail_cropping_rectangle(request, instance_pk=None):

    initial_queryset = ItemInstance.objects.filter(item_class__main_instance__id=F('id'))

    if request.GET.get('show_all') is None:
        initial_queryset = initial_queryset.filter(thumbnail_crop_size=0)

    return _set_cropping_rectangle(request,
        instance_pk=instance_pk,
        form_cls=ThumbnailCropForm,
        initial_queryset=initial_queryset,
        urlname='babytouch:set_thumbnail_cropping_rectangle',
        template_name='babytouch/set_thumbnail_cropping_rectangle.html',
    )


def itemclass_manage(request):

    classes_queryset = (ItemClass.objects.all()
        .order_by('group__id', '-is_enabled')
        .select_related('main_instance', 'group', 'main_instance__sound')
        .prefetch_related('instances'))

    for item_class in classes_queryset:
        item_class.instances_enabled_count = len(filter(lambda instance: instance.is_enabled, item_class.instances.all()))

    context = {
        'classes': classes_queryset,
    }

    return render(request, 'babytouch/itemclass_manage.html', context)


def itemclass_manage_single(request, pk):
    item_class = get_object_or_404(ItemClass, pk=pk)

    instances = item_class.instances.select_related('sound').order_by('-is_enabled')
    instances_total_count = len(instances)
    instances_enabled_count = len(filter(lambda i: i.is_enabled, instances))

    available_sounds = item_class.sounds.all()

    add_sound_form = AddSoundForm(initial={'item_class': item_class})

    context = {
        'item_class': item_class,
        'available_sounds': available_sounds,
        'add_sound_form': add_sound_form,
        'instances': instances,
        'instances_enabled_count': instances_enabled_count,
        'instances_total_count': instances_total_count,
    }

    return render(request, 'babytouch/itemclass_manage_single.html', context)


@require_http_methods(["POST"])
def itemclasssound_add(request):
    f = AddSoundForm(request.POST)
    if f.is_valid():
        f.save()

    return redirect(itemclass_manage_single, pk=f.instance.item_class_id)
