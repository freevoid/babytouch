import json
from functools import partial

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.http import require_http_methods
from .models import ItemInstance, ItemClass, Group, ItemClassSound


class JsonResponse(HttpResponse):

    content_type = 'text/javascript'

    def __init__(self, content='', status=200):
        if not isinstance(content, basestring):
            content = json.dumps(content)

        super(JsonResponse, self).__init__(content, content_type=self.content_type, status=status)


@require_http_methods(["POST"])
def common_toggle_enabled(request, pk, cls, is_enabled):
    nupdated = cls.objects.filter(pk=pk).update(is_enabled=is_enabled)
    if nupdated == 1:
        return JsonResponse()
    else:
        return JsonResponse(status=404)


group_enable = partial(common_toggle_enabled, cls=Group, is_enabled=True)
group_disable = partial(common_toggle_enabled, cls=Group, is_enabled=False)
itemclass_enable = partial(common_toggle_enabled, cls=ItemClass, is_enabled=True)
itemclass_disable = partial(common_toggle_enabled, cls=ItemClass, is_enabled=False)
iteminstance_enable = partial(common_toggle_enabled, cls=ItemInstance, is_enabled=True)
iteminstance_disable = partial(common_toggle_enabled, cls=ItemInstance, is_enabled=False)


@require_http_methods(["POST"])
def iteminstance_set_as_main_instance(request, pk):
    item_instance = get_object_or_404(ItemInstance, pk=pk)

    if item_instance.item_class_id is None:
        return JsonResponse(status=409)

    item_class = item_instance.item_class
    if item_class.main_instance_id == item_instance.pk:
        return JsonResponse()

    item_class.main_instance = item_instance
    item_class.save()
    return JsonResponse()


@require_http_methods(["POST"])
def iteminstance_set_sound(request, pk):
    item_instance = get_object_or_404(ItemInstance, pk=pk)

    if item_instance.item_class_id is None:
        return JsonResponse(status=409)

    sound_pk = request.POST.get('sound_id', None)

    if sound_pk is None:
        return JsonResponse(status=400)

    try:
        sound_pk = int(sound_pk)
    except ValueError:
        return JsonResponse(status=400)

    if item_instance.sound_id == sound_pk:
        return JsonResponse()

    available_sounds = ItemClassSound.objects.filter(item_class=item_instance.item_class)
    sound = get_object_or_404(available_sounds, pk=sound_pk)

    item_instance.sound = sound
    item_instance.save()

    return JsonResponse()
