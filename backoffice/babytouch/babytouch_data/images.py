from __future__ import division
import cStringIO as StringIO
import os
import sys
import functools
import fnmatch

import PIL
from django.core.files.base import ContentFile

from .models import ItemImage, IMAGE_SCALE


def is_size_fits_image(image_size, desired_size):
    return desired_size[0] <= image_size[0] and desired_size[1] <= image_size[1]


def get_resize_height(width, height, desired_width):
    return (desired_width * height) // width


def get_resize_size(width, height, desired_width):
    return desired_width, get_resize_height(width, height, desired_width)


def make_thumbnail(instance, is_force=False):
    if not instance.thumbnail_crop_size or (not is_force and instance.image_thumbnail):
        return

    buf = StringIO.StringIO()

    with open(instance.image.path, 'rb') as fp:
        image = PIL.Image.open(fp)
        image = crop_image(
            image,
            instance.thumbnail_crop_top,
            instance.thumbnail_crop_left,
            instance.thumbnail_crop_size,
            instance.thumbnail_crop_size)
        image.thumbnail((instance.THUMBNAIL_WIDTH, instance.THUMBNAIL_WIDTH), PIL.Image.ANTIALIAS)
        image.save(buf, 'JPEG')

    instance.image_thumbnail = ContentFile(buf.getvalue())
    thumbnail_name = instance.image_thumbnail.field.upload_to + '/thumb_' + os.path.basename(instance.image.name)
    instance.image_thumbnail.name = thumbnail_name
    instance.save()


def crop_image(image, top, left, width, height):
    return image.crop((left, top, left + width, top + height))


def create_asset_file(original, asset_type):
    buf = StringIO.StringIO()
    asset_size = IMAGE_SCALE.asset_sizes.get(asset_type)
    resized = original.resize(asset_size, PIL.Image.ANTIALIAS)
    resized.save(buf, 'JPEG')#, optimize=True)
    return ContentFile(buf.getvalue())


def generate_asset_name(original_name, asset_type):
    base_name, ext = os.path.splitext(original_name)
    return '%s.scale-%d%s' % (base_name, asset_type, ext)


def create_or_update_item_image(instance, cropped_image, scale_type):
    image_file = create_asset_file(cropped_image, scale_type)
    image_file.name = generate_asset_name(instance.image.name, scale_type)

    item_image, is_created = ItemImage.objects.get_or_create(instance=instance, scale=scale_type)
    if not is_created and item_image.image:
        item_image.image.delete()
    item_image.image = image_file
    item_image.save()
    return item_image


def create_image_assets(instance):
    assert bool(instance.crop_rectangle_width)
    assert bool(instance.image)

    original_image = PIL.Image.open(instance.image.path)

    cropped_image = crop_image(original_image,
        instance.crop_rectangle_top,
        instance.crop_rectangle_left,
        instance.crop_rectangle_width,
        instance.crop_rectangle_height)

    original_w, original_h = cropped_size = cropped_image.size

    if is_size_fits_image(cropped_size, IMAGE_SCALE.SCALE_100_SIZE):
        create_or_update_item_image(instance, cropped_image, IMAGE_SCALE.SCALE_100)
        if is_size_fits_image(cropped_size, IMAGE_SCALE.SCALE_140_SIZE):
            create_or_update_item_image(instance, cropped_image, IMAGE_SCALE.SCALE_140)
            if is_size_fits_image(cropped_size, IMAGE_SCALE.SCALE_180_SIZE):
                create_or_update_item_image(instance, cropped_image, IMAGE_SCALE.SCALE_180)
    else:
        print 'Image is too small: %s' % (cropped_size,)
        create_or_update_item_image(instance, cropped_image, IMAGE_SCALE.SCALE_100)
        #raise ValueError('Image is too small: %s' % (cropped_size,))


def optimize(image_path):
    cmd = 'jpegtran -copy none -optimize -progressive -outfile "%s" "%s"' % (image_path, image_path)
    return os.system(cmd)


def reduce_quality(image_path, quality=50):
    buf = StringIO.StringIO()

    with open(image_path, 'rb') as fp:
        image = PIL.Image.open(fp)
        image.save(buf, 'JPEG', quality=quality)

    with open(image_path, 'wb') as fp:
        fp.write(buf.getvalue())

    return True


def optimize_recursive(root, max_errors=3, stdout=sys.stdout):
    return perform_recursive(root, action=optimize, max_errors=max_errors, stdout=stdout)


def reduce_quality_recursive(root, max_errors=3, stdout=sys.stdout):
    return perform_recursive(root, action=reduce_quality, max_errors=max_errors, stdout=stdout)


def perform_recursive(root, action, max_errors=3, stdout=sys.stdout):
    nerrors = 0
    nprocessed = 0
    is_jpeg = functools.partial(fnmatch.fnmatch, pat="*.jpg")
    for dirpath, dirnames, filenames in os.walk(root):
        for filename in filter(is_jpeg, filenames):
            image_path = os.path.join(dirpath, filename)
            error = action(image_path)
            if error:
                nerrors += 1
                if nerrors >= max_errors:
                    stdout.write("MAXIMUM ERRORS REACHED, ABORTING")
                    return 0
            else:
                nerrors = 0
                nprocessed += 1
                if nprocessed % 10 == 0:
                    stdout.write("Processed images: %4d" % nprocessed)
                    stdout.flush()

    return nprocessed
