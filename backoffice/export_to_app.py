#!/usr/bin/env python
from __future__ import unicode_literals
import os
import sys
import shutil

current_dir = os.path.dirname(os.path.abspath(__file__))
get_relative_path = lambda *path: os.path.join(current_dir, *path)

APP_PROJECT_PATH = get_relative_path('..', 'app', 'BabyTouch')
APP_CONTENT_PATH = os.path.join(APP_PROJECT_PATH, 'DefaultContent')
EXPORT_DIR_PATH = get_relative_path('export')


def export_to_exports_directory(args):
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "babytouch.settings")
    from django.core.management import execute_from_command_line
    execute_from_command_line(["manage.py", "export_to_babytouch"] + args)


def copy_exported_resources_to_app(exported_path=None):
    if exported_path is None:
        last_exported_path = max(os.listdir(EXPORT_DIR_PATH))

        if last_exported_path.startswith('.'):
            print 'Could not find any exported data under "%s". Check the export command output.' % EXPORT_DIR_PATH
            return False

        exported_path = os.path.join(EXPORT_DIR_PATH, last_exported_path)

    if os.path.exists(APP_CONTENT_PATH):
        shutil.rmtree(APP_CONTENT_PATH)
    shutil.copytree(exported_path, APP_CONTENT_PATH)


def print_usage():
    print 'python export_to_app.py (copy <path-to-exported-data> | export <manage.py-export-parameters>)'


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print_usage()
        sys.exit(1)

    if sys.argv[1] == "copy":
        print 'Deploying resources to the app project directory..'
        copy_exported_resources_to_app(sys.argv[2] if len(sys.argv) >= 3 else None)
    elif sys.argv[1] == "export":
        print 'Exporting resources from database..'
        export_to_exports_directory(sys.argv[2:])
        print 'Deploying resources to the app project directory..'
        copy_exported_resources_to_app()
        print 'Done.'
    else:
        print_usage()
        sys.exit(1)
